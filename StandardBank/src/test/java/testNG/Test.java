package testNG;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;


class TestNGTestOne extends CrossBrowserScript {
    public static void main(String[] args) throws InterruptedException {
        TestNGTestOne obj = new TestNGTestOne();
        obj.login();
    }

    //WebDriver driver;

    @Test
    public void login() throws InterruptedException {

        //Setting up the chrome driver exe, the second argument is the location where you have kept the driver in your system

       // System.setProperty("webdriver.chrome.driver", ".\\chromedriver.exe");
        //Setting the driver to chrome driver
        //driver = new ChromeDriver();
        String url = "https://www.google.com";
        driver.get(url);
        Thread.sleep(1000);
        driver.manage().window().maximize();
        //Capturing the title and validating if expected is equal to actual
        String expectedTitle = "Google";
        String actualTitle = driver.getTitle();
        System.out.println("Assert passed");
        Assert.assertEquals(actualTitle, expectedTitle);

    }

    @BeforeMethod

    public void beforeMethod() {

        System.out.println("Starting the browser session");

    }

    @AfterMethod
    public void afterMethod() {

        System.out.println("Closing the browser session");
        driver.close();

    }
}