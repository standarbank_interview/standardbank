import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

class practise {

    WebDriver driver ;
    @BeforeTest
    @Parameters("browser")

    public void settings(String browser) throws Exception {
        if(browser.equalsIgnoreCase("chrome"))
        {
            System.setProperty("webdirver.chrome.driver",".\\chromedriver.exe");
            driver = new ChromeDriver();
        }
        else if(browser.equalsIgnoreCase("firefox"))
        {
            System.setProperty("webdriver.gecko.driver",".\\geckodriver.exe");
            driver = new FirefoxDriver();
        }
        else if(browser.equalsIgnoreCase("Edge"))
        {
            System.setProperty("webdriver.edge.driver",".\\MicrosoftWebDriver.exe");
            driver = new EdgeDriver();
        }

        else
        {
            throw new Exception("No browser found");
        }
    }
}
